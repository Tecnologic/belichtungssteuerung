#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-XC8_PIC16F887.mk)" "nbproject/Makefile-local-XC8_PIC16F887.mk"
include nbproject/Makefile-local-XC8_PIC16F887.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=XC8_PIC16F887
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/BelichtungsSteuerung.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/BelichtungsSteuerung.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=configuration_bits.c ds1820.c encoder.c interrupts.c main.c system.c user.c hitachilcd-ak/lcd.c menu.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/configuration_bits.p1 ${OBJECTDIR}/ds1820.p1 ${OBJECTDIR}/encoder.p1 ${OBJECTDIR}/interrupts.p1 ${OBJECTDIR}/main.p1 ${OBJECTDIR}/system.p1 ${OBJECTDIR}/user.p1 ${OBJECTDIR}/hitachilcd-ak/lcd.p1 ${OBJECTDIR}/menu.p1
POSSIBLE_DEPFILES=${OBJECTDIR}/configuration_bits.p1.d ${OBJECTDIR}/ds1820.p1.d ${OBJECTDIR}/encoder.p1.d ${OBJECTDIR}/interrupts.p1.d ${OBJECTDIR}/main.p1.d ${OBJECTDIR}/system.p1.d ${OBJECTDIR}/user.p1.d ${OBJECTDIR}/hitachilcd-ak/lcd.p1.d ${OBJECTDIR}/menu.p1.d

# Object Files
OBJECTFILES=${OBJECTDIR}/configuration_bits.p1 ${OBJECTDIR}/ds1820.p1 ${OBJECTDIR}/encoder.p1 ${OBJECTDIR}/interrupts.p1 ${OBJECTDIR}/main.p1 ${OBJECTDIR}/system.p1 ${OBJECTDIR}/user.p1 ${OBJECTDIR}/hitachilcd-ak/lcd.p1 ${OBJECTDIR}/menu.p1

# Source Files
SOURCEFILES=configuration_bits.c ds1820.c encoder.c interrupts.c main.c system.c user.c hitachilcd-ak/lcd.c menu.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
	${MAKE} ${MAKE_OPTIONS} -f nbproject/Makefile-XC8_PIC16F887.mk dist/${CND_CONF}/${IMAGE_TYPE}/BelichtungsSteuerung.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=16F887
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/configuration_bits.p1: configuration_bits.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/configuration_bits.p1.d 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --opt=default,+asm,-asmfile,+speed,-space,+debug --addrqual=ignore --mode=pro -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.12/include" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X/hitachilcd-ak" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,+osccal,+resetbits,-download,+stackcall,+clib --output=-mcof,+elf "--errformat=%%f:%%l: error: %%s" "--warnformat=%%f:%%l: warning: %%s" "--msgformat=%%f:%%l: advisory: %%s"  -o${OBJECTDIR}/configuration_bits.p1  configuration_bits.c 
	@-${MV} ${OBJECTDIR}/configuration_bits.d ${OBJECTDIR}/configuration_bits.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/configuration_bits.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ds1820.p1: ds1820.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/ds1820.p1.d 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --opt=default,+asm,-asmfile,+speed,-space,+debug --addrqual=ignore --mode=pro -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.12/include" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X/hitachilcd-ak" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,+osccal,+resetbits,-download,+stackcall,+clib --output=-mcof,+elf "--errformat=%%f:%%l: error: %%s" "--warnformat=%%f:%%l: warning: %%s" "--msgformat=%%f:%%l: advisory: %%s"  -o${OBJECTDIR}/ds1820.p1  ds1820.c 
	@-${MV} ${OBJECTDIR}/ds1820.d ${OBJECTDIR}/ds1820.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ds1820.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/encoder.p1: encoder.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/encoder.p1.d 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --opt=default,+asm,-asmfile,+speed,-space,+debug --addrqual=ignore --mode=pro -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.12/include" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X/hitachilcd-ak" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,+osccal,+resetbits,-download,+stackcall,+clib --output=-mcof,+elf "--errformat=%%f:%%l: error: %%s" "--warnformat=%%f:%%l: warning: %%s" "--msgformat=%%f:%%l: advisory: %%s"  -o${OBJECTDIR}/encoder.p1  encoder.c 
	@-${MV} ${OBJECTDIR}/encoder.d ${OBJECTDIR}/encoder.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/encoder.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/interrupts.p1: interrupts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/interrupts.p1.d 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --opt=default,+asm,-asmfile,+speed,-space,+debug --addrqual=ignore --mode=pro -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.12/include" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X/hitachilcd-ak" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,+osccal,+resetbits,-download,+stackcall,+clib --output=-mcof,+elf "--errformat=%%f:%%l: error: %%s" "--warnformat=%%f:%%l: warning: %%s" "--msgformat=%%f:%%l: advisory: %%s"  -o${OBJECTDIR}/interrupts.p1  interrupts.c 
	@-${MV} ${OBJECTDIR}/interrupts.d ${OBJECTDIR}/interrupts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/interrupts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/main.p1: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/main.p1.d 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --opt=default,+asm,-asmfile,+speed,-space,+debug --addrqual=ignore --mode=pro -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.12/include" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X/hitachilcd-ak" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,+osccal,+resetbits,-download,+stackcall,+clib --output=-mcof,+elf "--errformat=%%f:%%l: error: %%s" "--warnformat=%%f:%%l: warning: %%s" "--msgformat=%%f:%%l: advisory: %%s"  -o${OBJECTDIR}/main.p1  main.c 
	@-${MV} ${OBJECTDIR}/main.d ${OBJECTDIR}/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/system.p1: system.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/system.p1.d 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --opt=default,+asm,-asmfile,+speed,-space,+debug --addrqual=ignore --mode=pro -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.12/include" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X/hitachilcd-ak" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,+osccal,+resetbits,-download,+stackcall,+clib --output=-mcof,+elf "--errformat=%%f:%%l: error: %%s" "--warnformat=%%f:%%l: warning: %%s" "--msgformat=%%f:%%l: advisory: %%s"  -o${OBJECTDIR}/system.p1  system.c 
	@-${MV} ${OBJECTDIR}/system.d ${OBJECTDIR}/system.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/system.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/user.p1: user.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/user.p1.d 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --opt=default,+asm,-asmfile,+speed,-space,+debug --addrqual=ignore --mode=pro -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.12/include" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X/hitachilcd-ak" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,+osccal,+resetbits,-download,+stackcall,+clib --output=-mcof,+elf "--errformat=%%f:%%l: error: %%s" "--warnformat=%%f:%%l: warning: %%s" "--msgformat=%%f:%%l: advisory: %%s"  -o${OBJECTDIR}/user.p1  user.c 
	@-${MV} ${OBJECTDIR}/user.d ${OBJECTDIR}/user.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/user.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/hitachilcd-ak/lcd.p1: hitachilcd-ak/lcd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/hitachilcd-ak 
	@${RM} ${OBJECTDIR}/hitachilcd-ak/lcd.p1.d 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --opt=default,+asm,-asmfile,+speed,-space,+debug --addrqual=ignore --mode=pro -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.12/include" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X/hitachilcd-ak" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,+osccal,+resetbits,-download,+stackcall,+clib --output=-mcof,+elf "--errformat=%%f:%%l: error: %%s" "--warnformat=%%f:%%l: warning: %%s" "--msgformat=%%f:%%l: advisory: %%s"  -o${OBJECTDIR}/hitachilcd-ak/lcd.p1  hitachilcd-ak/lcd.c 
	@-${MV} ${OBJECTDIR}/hitachilcd-ak/lcd.d ${OBJECTDIR}/hitachilcd-ak/lcd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/hitachilcd-ak/lcd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/menu.p1: menu.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/menu.p1.d 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --opt=default,+asm,-asmfile,+speed,-space,+debug --addrqual=ignore --mode=pro -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.12/include" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X/hitachilcd-ak" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,+osccal,+resetbits,-download,+stackcall,+clib --output=-mcof,+elf "--errformat=%%f:%%l: error: %%s" "--warnformat=%%f:%%l: warning: %%s" "--msgformat=%%f:%%l: advisory: %%s"  -o${OBJECTDIR}/menu.p1  menu.c 
	@-${MV} ${OBJECTDIR}/menu.d ${OBJECTDIR}/menu.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/menu.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
else
${OBJECTDIR}/configuration_bits.p1: configuration_bits.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/configuration_bits.p1.d 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --opt=default,+asm,-asmfile,+speed,-space,+debug --addrqual=ignore --mode=pro -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.12/include" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X/hitachilcd-ak" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,+osccal,+resetbits,-download,+stackcall,+clib --output=-mcof,+elf "--errformat=%%f:%%l: error: %%s" "--warnformat=%%f:%%l: warning: %%s" "--msgformat=%%f:%%l: advisory: %%s"  -o${OBJECTDIR}/configuration_bits.p1  configuration_bits.c 
	@-${MV} ${OBJECTDIR}/configuration_bits.d ${OBJECTDIR}/configuration_bits.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/configuration_bits.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/ds1820.p1: ds1820.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/ds1820.p1.d 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --opt=default,+asm,-asmfile,+speed,-space,+debug --addrqual=ignore --mode=pro -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.12/include" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X/hitachilcd-ak" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,+osccal,+resetbits,-download,+stackcall,+clib --output=-mcof,+elf "--errformat=%%f:%%l: error: %%s" "--warnformat=%%f:%%l: warning: %%s" "--msgformat=%%f:%%l: advisory: %%s"  -o${OBJECTDIR}/ds1820.p1  ds1820.c 
	@-${MV} ${OBJECTDIR}/ds1820.d ${OBJECTDIR}/ds1820.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/ds1820.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/encoder.p1: encoder.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/encoder.p1.d 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --opt=default,+asm,-asmfile,+speed,-space,+debug --addrqual=ignore --mode=pro -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.12/include" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X/hitachilcd-ak" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,+osccal,+resetbits,-download,+stackcall,+clib --output=-mcof,+elf "--errformat=%%f:%%l: error: %%s" "--warnformat=%%f:%%l: warning: %%s" "--msgformat=%%f:%%l: advisory: %%s"  -o${OBJECTDIR}/encoder.p1  encoder.c 
	@-${MV} ${OBJECTDIR}/encoder.d ${OBJECTDIR}/encoder.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/encoder.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/interrupts.p1: interrupts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/interrupts.p1.d 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --opt=default,+asm,-asmfile,+speed,-space,+debug --addrqual=ignore --mode=pro -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.12/include" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X/hitachilcd-ak" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,+osccal,+resetbits,-download,+stackcall,+clib --output=-mcof,+elf "--errformat=%%f:%%l: error: %%s" "--warnformat=%%f:%%l: warning: %%s" "--msgformat=%%f:%%l: advisory: %%s"  -o${OBJECTDIR}/interrupts.p1  interrupts.c 
	@-${MV} ${OBJECTDIR}/interrupts.d ${OBJECTDIR}/interrupts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/interrupts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/main.p1: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/main.p1.d 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --opt=default,+asm,-asmfile,+speed,-space,+debug --addrqual=ignore --mode=pro -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.12/include" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X/hitachilcd-ak" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,+osccal,+resetbits,-download,+stackcall,+clib --output=-mcof,+elf "--errformat=%%f:%%l: error: %%s" "--warnformat=%%f:%%l: warning: %%s" "--msgformat=%%f:%%l: advisory: %%s"  -o${OBJECTDIR}/main.p1  main.c 
	@-${MV} ${OBJECTDIR}/main.d ${OBJECTDIR}/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/system.p1: system.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/system.p1.d 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --opt=default,+asm,-asmfile,+speed,-space,+debug --addrqual=ignore --mode=pro -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.12/include" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X/hitachilcd-ak" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,+osccal,+resetbits,-download,+stackcall,+clib --output=-mcof,+elf "--errformat=%%f:%%l: error: %%s" "--warnformat=%%f:%%l: warning: %%s" "--msgformat=%%f:%%l: advisory: %%s"  -o${OBJECTDIR}/system.p1  system.c 
	@-${MV} ${OBJECTDIR}/system.d ${OBJECTDIR}/system.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/system.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/user.p1: user.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/user.p1.d 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --opt=default,+asm,-asmfile,+speed,-space,+debug --addrqual=ignore --mode=pro -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.12/include" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X/hitachilcd-ak" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,+osccal,+resetbits,-download,+stackcall,+clib --output=-mcof,+elf "--errformat=%%f:%%l: error: %%s" "--warnformat=%%f:%%l: warning: %%s" "--msgformat=%%f:%%l: advisory: %%s"  -o${OBJECTDIR}/user.p1  user.c 
	@-${MV} ${OBJECTDIR}/user.d ${OBJECTDIR}/user.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/user.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/hitachilcd-ak/lcd.p1: hitachilcd-ak/lcd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/hitachilcd-ak 
	@${RM} ${OBJECTDIR}/hitachilcd-ak/lcd.p1.d 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --opt=default,+asm,-asmfile,+speed,-space,+debug --addrqual=ignore --mode=pro -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.12/include" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X/hitachilcd-ak" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,+osccal,+resetbits,-download,+stackcall,+clib --output=-mcof,+elf "--errformat=%%f:%%l: error: %%s" "--warnformat=%%f:%%l: warning: %%s" "--msgformat=%%f:%%l: advisory: %%s"  -o${OBJECTDIR}/hitachilcd-ak/lcd.p1  hitachilcd-ak/lcd.c 
	@-${MV} ${OBJECTDIR}/hitachilcd-ak/lcd.d ${OBJECTDIR}/hitachilcd-ak/lcd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/hitachilcd-ak/lcd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/menu.p1: menu.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/menu.p1.d 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=24 --float=24 --opt=default,+asm,-asmfile,+speed,-space,+debug --addrqual=ignore --mode=pro -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.12/include" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X/hitachilcd-ak" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,+osccal,+resetbits,-download,+stackcall,+clib --output=-mcof,+elf "--errformat=%%f:%%l: error: %%s" "--warnformat=%%f:%%l: warning: %%s" "--msgformat=%%f:%%l: advisory: %%s"  -o${OBJECTDIR}/menu.p1  menu.c 
	@-${MV} ${OBJECTDIR}/menu.d ${OBJECTDIR}/menu.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/menu.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/BelichtungsSteuerung.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) --chip=$(MP_PROCESSOR_OPTION) -G -mdist/${CND_CONF}/${IMAGE_TYPE}/BelichtungsSteuerung.X.${IMAGE_TYPE}.map  -D__DEBUG=1 --debugger=pickit3  --double=24 --float=24 --opt=default,+asm,-asmfile,+speed,-space,+debug --addrqual=ignore --mode=pro -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.12/include" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X/hitachilcd-ak" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,+osccal,+resetbits,-download,+stackcall,+clib --output=-mcof,+elf "--errformat=%%f:%%l: error: %%s" "--warnformat=%%f:%%l: warning: %%s" "--msgformat=%%f:%%l: advisory: %%s"   --rom=default,-1f00-1ffe --ram=default,-0-0,-70-70,-80-80,-f0-f0,-100-100,-170-170,-180-180,-1e5-1f0  -odist/${CND_CONF}/${IMAGE_TYPE}/BelichtungsSteuerung.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/BelichtungsSteuerung.X.${IMAGE_TYPE}.hex 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/BelichtungsSteuerung.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) --chip=$(MP_PROCESSOR_OPTION) -G -mdist/${CND_CONF}/${IMAGE_TYPE}/BelichtungsSteuerung.X.${IMAGE_TYPE}.map  --double=24 --float=24 --opt=default,+asm,-asmfile,+speed,-space,+debug --addrqual=ignore --mode=pro -P -N255 -I"C:/Program Files (x86)/Microchip/xc8/v1.12/include" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"D:/Elektronik/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X/hitachilcd-ak" -I"C:/Users/Alex/Documents/GitHub/BelichtungsSteuerung/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X" -I"D:/Projekte/Privat/BelichtungsSteuerung-master/BelichtungsSteuerung.X/hitachilcd-ak" --warn=0 --asmlist --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,+osccal,+resetbits,-download,+stackcall,+clib --output=-mcof,+elf "--errformat=%%f:%%l: error: %%s" "--warnformat=%%f:%%l: warning: %%s" "--msgformat=%%f:%%l: advisory: %%s"   -odist/${CND_CONF}/${IMAGE_TYPE}/BelichtungsSteuerung.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/XC8_PIC16F887
	${RM} -r dist/XC8_PIC16F887

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
