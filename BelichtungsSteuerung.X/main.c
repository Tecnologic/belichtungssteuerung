/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#endif

#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "user.h"
#include "lcd.h"
#include "encoder.h"          /* User funct/params, such as InitApp */
#include "ds1820.h"
#include "menu.h"

/******************************************************************************/
/* User Global Variable Declaration                                           */
/******************************************************************************/

/* i.e. uint8_t <variable_name>; */

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/
void main(void)
{
    static char buffer[25];
    /* Configure the oscillator for the device */
    //ConfigureOscillator();

    /* Initialize I/O and Peripherals for application */
    InitApp();

    OUT_LED1 = 0;
    LCD_BL  = 1;
    lcd_clear();
    lcd_gotoxy(1,1);
    lcd_printf("Belichter 2013      ");
    lcd_gotoxy(2,1);
    lcd_printf("booting...          ");

    /* init DS1820 sensors */
    if(true == DS1820_FindFirstDevice())
    {
        lcd_clear();
        lcd_gotoxy(1,1);
        lcd_printf("DS1820 gefunden     ");
        lcd_gotoxy(2,1);
        sprintf(buffer,"ID:   %#X%X%X%X%X%X%X%X", nRomAddr_au8[7], nRomAddr_au8[6],
                nRomAddr_au8[5], nRomAddr_au8[4], nRomAddr_au8[3],
                nRomAddr_au8[2], nRomAddr_au8[1], nRomAddr_au8[0]);
        lcd_printf(buffer);
        DS1820_StartTempConv();
        __delay_ms(1000);
        /* first read of temp is 0 */
        DS1820_GetTempRaw();
        /* start next conversion */
        DS1820_StartTempConv();
        /* beep for success */
        OUT_BUZZER = para.Beep&0x01;
        OUT_LED2 = 0;
        __delay_ms(100);
        OUT_BUZZER = 0;
        /* show the ds1820 data to the user */
        __delay_ms(500);

    }else{
        lcd_clear();
        lcd_gotoxy(1,1);
        lcd_printf("kein DS1820 gefunden");
        para.TempSensor = false;
    }



    lcd_clear();
    lcd_gotoxy(1,1);
    lcd_printf("Belichter 2013!     ");

    /* beep for success */
    OUT_BUZZER = para.Beep&0x01;
    __delay_ms(50);
    OUT_BUZZER = 0;

    PEIE = 1;
    ei();

    while(1)
    {
        mnu_DoMenu();

    }

}

