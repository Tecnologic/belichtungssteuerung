/* 
 * File:   encoder.h
 * Author: A
 *
 * Created on 18. M�rz 2013, 22:35
 */

#ifndef ENCODER_H
#define	ENCODER_H

#ifdef	__cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
#define ENC_PIN_A     PORTBbits.RB2
#define ENC_PIN_B     PORTBbits.RB1
#define ENC_BUTTON    PORTBbits.RB0

#define ENC_PIN_A_TRIS     TRISBbits.TRISB2
#define ENC_PIN_B_TRIS     TRISBbits.TRISB1
#define ENC_BUTTON_TRIS    TRISBbits.TRISB0

#define ENC_RATE                    500 /* ms sample cycle for encoder inputs */
#define ENC_BUTTON_PRESS_THRESHOLD  ENC_RATE /* 100 bei 100Hz f�r 1s */
/* Exported macro ------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
extern volatile int8_t cEncDelta;
extern volatile int8_t cLast;

#if 0
extern volatile bool bEncBtnShrtPrs;
extern volatile bool bEncBtnLngPrs;
#else
/* PIC16 sucks */
extern volatile int8_t cEncDelta;
extern volatile int8_t cLast;
extern volatile uint8_t uButtonStateOld;
extern volatile uint8_t uButtonState;
extern volatile uint16_t uButtonPressCnt;
extern volatile bool bEncBtnShrtPrs;
extern volatile bool bEncBtnLngPrs;
extern volatile bool bEncBtnLngPrsOld;
extern const int8_t iEncTable[16];
#endif
 /*******************************************************************************
 * FUNCTION:   Encoder_Init
 * PURPOSE:    initialise encoder inputs and timer setup
 *
 * INPUT:      -
 * OUTPUT:     -
 * RETURN:     -
 ******************************************************************************/
inline void Encoder_Init(void);

/*******************************************************************************
 * FUNCTION:   Encoder_Handler
 * PURPOSE:    handels encoder signals, needs to be called regularly
 *             see http://www.mikrocontroller.net/articles/Drehgeber
 *
 * INPUT:      -
 * OUTPUT:     -
 * RETURN:     -
 ******************************************************************************/
inline void Encoder_Handler(void);

/*******************************************************************************
 * FUNCTION:   Encoder_Read
 * PURPOSE:    decode encoder incrementals
 *             see http://www.mikrocontroller.net/articles/Drehgeber
 *
 * INPUT:      -
 * OUTPUT:     -
 * RETURN:     increment by encoder signals
 ******************************************************************************/
inline int8_t Encoder_Read(void);

/*******************************************************************************
 * FUNCTION:   Encoder_ShrtBtnPress
 * PURPOSE:    check for short button press
 *
 * INPUT:      -
 * OUTPUT:     -
 * RETURN:     true on short button press
 ******************************************************************************/
inline bool Encoder_ShrtBtnPress(void);

/*******************************************************************************
 * FUNCTION:   Encoder_LngBtnPress
 * PURPOSE:    check for long button press
 *
 * INPUT:      -
 * OUTPUT:     -
 * RETURN:     true on long button press
 ******************************************************************************/
inline bool Encoder_LngBtnPress(void);


#ifdef	__cplusplus
}
#endif

#endif	/* ENCODER_H */

