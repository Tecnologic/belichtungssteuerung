/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#endif

#include "ds1820.h"
#include "system.h"
#include "encoder.h"

#include "user.h"
#include "lcd.h"


/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

/* <Initialize variables in user.h and insert code for user algorithms.> */

inline void InitApp(void)
{

    /* Initialize User Ports/Peripherals/Project */
    ANSEL = 0;
    ANSELH = 0;
    TRISEbits.TRISE0 = 1;
    TRISEbits.TRISE2 = 1;
    /* Setup analog functionality and port direction */
    /* Relay output */
    PORTAbits.RA3 = 0;
    TRISAbits.TRISA3 = 0;
    /* Positioning lights */
    PORTCbits.RC2 = 0;
    TRISCbits.TRISC2 = 0;
    /* BUZZER output */
    PORTBbits.RB4 = 0;
    TRISBbits.TRISB4 = 0;
    /* LED Outputs */
    PORTAbits.RA0 = 0;
    TRISAbits.TRISA0 = 0;
    PORTAbits.RA2 = 0;
    TRISAbits.TRISA2 = 0;
    PORTAbits.RA4 = 0;
    TRISAbits.TRISA4 = 0;
    /* Initialize peripherals */
    /* initialize Encoder */
    Encoder_Init();
    
    /* init LCD */
    lcd_init();
  

    /* Enable interrupts */
}

