/* 
 * File:   state.h
 * Author: A
 *
 * Created on 9. April 2013, 19:36
 */

#ifndef STATE_H
#define	STATE_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "system.h"

/* No State */
#define STATE_UNDEF             0

/* transition type  */
typedef struct {
    uint8_t actualState; /* start of transition state */
    bool (*TransitionCheck)(void); /* check function, make transition on true */
    void (*TransitionAction)(void);/* action when Transition true */
    uint8_t nextState;  /* end of transition state */
}s_Transition;

/* special end transition, needs to be the last element of every Transition table*/
s_Transition endOfTransitionTable ={STATE_UNDEF, NULL, NULL,STATE_UNDEF};


#ifdef	__cplusplus
}
#endif

#endif	/* STATE_H */

