
#ifndef SYSTEM_H
#define	SYSTEM_H

/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/
#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>


/******************************************************************************/
/* System Level #define Macros                                                */
/******************************************************************************/

/* Microcontroller MIPs (FCY) */
#define SYS_FREQ                20000000L
#define _XTAL_FREQ              SYS_FREQ
#define FCY                     SYS_FREQ/4
#define DS1820_DATAPIN_PORT     PORTCbits.RC0
#define DS1820_DATAPIN_TRIS     TRISCbits.TRISC0
#define OUT_BUZZER              PORTBbits.RB4
#define OUT_POS_LIGHT           PORTCbits.RC2
#define OUT_UV_LIGHT            PORTAbits.RA3
#define OUT_LED1                PORTAbits.RA0
#define OUT_LED2                PORTAbits.RA2
#define OUT_LED3                PORTAbits.RA4
#define IN_HOOD                 PORTEbits.RE0
#define IN_RESERVE              PORTEbits.RE2

extern volatile bool bOneSec;
/******************************************************************************/
/* System Function Prototypes                                                 */
/******************************************************************************/

/* Custom oscillator configuration funtions, reset source evaluation
functions, and other non-peripheral microcontroller initialization functions
go here. */

void ConfigureOscillator(void); /* Handles clock switching/osc initialization */

#endif /*SYSTEM_H*/