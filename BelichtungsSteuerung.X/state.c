/*
 * File:   state.c
 * Author: A
 *
 * Created on 9. April 2013, 19:36
 */

#ifndef STATE_C
#define	STATE_C

#ifdef	__cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "state.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
volatile uint8_t uState = 0;
volatile uint8_t uStateOld = 0;
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Global functions ----------------------------------------------------------*/

 /******************************************************************************
 * FUNCTION:   State_Do
 * PURPOSE:    run though statemachine
 *
 * INPUT:      Transition table
 * OUTPUT:     -
 * RETURN:     -
 ******************************************************************************/
void State_Do(s_Transition* transitionTable){
    /* check for end of transition table */
    while(transitionTable->nextState == STATE_UNDEF){
        /* check for transition from this State */
        if(transitionTable->actualState == uState){
            if(transitionTable->TransitionCheck() == true){
                /* transition check is ture so we perform transition */
                uState = transitionTable->nextState;
                transitionTable->TransitionAction();
            }
        }
        /* on state change leave the loop and come back here in the next cycle*/
        if(uStateOld != uState){
            break;
        }
   }
   uStateOld = uState;
}

#ifdef	__cplusplus
}
#endif

#endif	/* STATE_C */

