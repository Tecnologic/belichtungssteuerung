/*
 * File:   encoder.c
 * Author: A
 *
 * Created on 18. M�rz 2013, 22:35
 */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef ENCODER_C
#define	ENCODER_C

#ifdef	__cplusplus
extern "C" {
#endif
/* Includes ------------------------------------------------------------------*/
#include <xc.h>
#include "system.h"
#include "encoder.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
volatile int8_t cEncDelta = 0;
volatile int8_t cLast = 0;
volatile uint8_t uButtonStateOld = 0;
volatile uint8_t uButtonState = 0;
volatile uint16_t uButtonPressCnt = 0;
volatile bool bEncBtnShrtPrs = false;
volatile bool bEncBtnLngPrs = false;
volatile bool bEncBtnLngPrsOld = false;

const int8_t iEncTable[16] = {0,0,0,0,-1,0,1,0,0,0,0,0,1,0,-1,0};
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Global functions ----------------------------------------------------------*/

 /*******************************************************************************
 * FUNCTION:   Encoder_Init
 * PURPOSE:    initialise encoder inputs and timer setup
 *
 * INPUT:      -
 * OUTPUT:     -
 * RETURN:     -
 ******************************************************************************/
inline void Encoder_Init(void){
    int8_t cNew;

    ENC_PIN_A_TRIS  = 1;
    ENC_PIN_B_TRIS = 1;
    ENC_BUTTON_TRIS = 1;

       /* encoder 2-bit gray code decoding */
    cNew = 0;
#if 1
    if(ENC_PIN_A  != 0){
        cNew = 3;
    }
    if(ENC_PIN_B != 0){
        cNew ^= 1;
    }
    /* save init state */
    cLast = cNew;
    cEncDelta = 0;
#endif
    
    T1CONbits.T1CKPS = 0; /* 1:1 PSC */
    T1CONbits.TMR1CS = 0; /* internal clock source */
    CCPR1 = (FCY / ENC_RATE )-1; /* fix sample rate */
    CCP1CONbits.CCP1M = 0b1010; /* generate SW interrupt on match */
    CCP1IF = 0;  /* clear interupt flag */
    CCP1IE = 1;  /* enable timer interupt*/
    T1CONbits.TMR1ON = 1; /* enable Timer */

 
}

/*******************************************************************************
 * FUNCTION:   Encoder_Handler
 * PURPOSE:    handles encoder inputs, should be called in the isr routine
 *             see http://www.mikrocontroller.net/articles/Drehgeber
 *
 * INPUT:      -
 * OUTPUT:     -
 * RETURN:     -
 ******************************************************************************/
inline void Encoder_Handler(void){

    int8_t cNew = 0;

    /* build binary from gray */
    if(ENC_PIN_A  != 0){
        cNew = 3;
    }
    if(ENC_PIN_B != 0){
        cNew ^= 1;
    }
    /* shift old value up */
    cLast = (cLast << 2)  & 0x0F;
    /* add new binary value */
    cLast += cNew;

    cEncDelta += iEncTable[cLast];

    /* Encoder-Taster Pr�fen */
    uButtonState = (ENC_BUTTON == 0);/*Invertierung des Tasters korrigieren*/
    if(uButtonState){
        /* Dauer des Knopf Dr�ckens z�hlen */
        uButtonPressCnt++;
    }
    if((uButtonState != uButtonStateOld) && (uButtonState == 0)){
        /* negative Flanke am Taster */
        if(uButtonPressCnt < ENC_BUTTON_PRESS_THRESHOLD){
            /* Kurzer Tastendruck */
            bEncBtnShrtPrs = true;
        }
        uButtonPressCnt = 0;
        bEncBtnLngPrsOld = false;
    }

    if(uButtonPressCnt >= ENC_BUTTON_PRESS_THRESHOLD){
        /* Ein Langer Tastendruck wurde erkannt */
        if(bEncBtnLngPrsOld == false){
            bEncBtnLngPrs = true;
            bEncBtnLngPrsOld = true;
        }
    }
    uButtonStateOld = uButtonState;
}

/*******************************************************************************
 * FUNCTION:   Encoder_Read
 * PURPOSE:    handles encoder inputs, should be called in the isr routine
 *             see http://www.mikrocontroller.net/articles/Drehgeber
 *
 * INPUT:      -
 * OUTPUT:     -
 * RETURN:     -
 ******************************************************************************/
inline int8_t Encoder_Read(void){
    int8_t cValue;
    
    /* prevent timer interupts */
    di();
    cValue = cEncDelta;
    cEncDelta = 0;
    /* reenable interupts */
    ei();
    return (cValue);
}

/*******************************************************************************
 * FUNCTION:   Encoder_ShrtBtnPress
 * PURPOSE:    check for short button press
 *
 * INPUT:      -
 * OUTPUT:     -
 * RETURN:     true on short button press
 ******************************************************************************/
inline bool Encoder_ShrtBtnPress(void){
    if(bEncBtnShrtPrs){
        /* pull down flag */
        bEncBtnShrtPrs = false;
        return true;
    }
    return false;
}

/*******************************************************************************
 * FUNCTION:   Encoder_LngBtnPress
 * PURPOSE:    check for long button press
 *
 * INPUT:      -
 * OUTPUT:     -
 * RETURN:     true on long button press
 ******************************************************************************/
inline bool Encoder_LngBtnPress(void){
    if(bEncBtnLngPrs){
        /* pull down flag */
        bEncBtnLngPrs = false;
        return true;
    }
    return false;
}
#ifdef	__cplusplus
}
#endif

#endif	/* ENCODER_C */


