/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef INTERRUPTS_C
#define	INTERRUPTS_C

#ifdef	__cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "system.h"
#include "encoder.h"
#include "ds1820.h"
#include <xc.h>
#include <GenericTypeDefs.h>
    
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
volatile uint16_t uCycleCount = 0;
volatile bool bOneSec = false;
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Global functions ----------------------------------------------------------*/

/******************************************************************************/
/* Interrupt Routines                                                         */
/******************************************************************************/

/* Baseline devices don't have interrupts. Note that some PIC16's 
 * are baseline devices.  Unfortunately the baseline detection macro is 
 * _PIC12 */
#ifndef _PIC12

void interrupt high_isr (){
    int8_t cNew, cDiff;
    
    /* This code stub shows general interrupt handling.  Note that these
    conditional statements are not handled within 3 seperate if blocks.
    Do not use a seperate if block for each interrupt flag to avoid run
    time errors. */
    /* check for timer compare interrupt */
    if(CCP1IF != 0){
        /* reset timer at first to maintain cycle time */
        TMR1 = 0;
        CCP1IF = 0;
        /* Handle Encoder */
#if 0
        Encoder_Handler();
#else
        /* PIC16 sucks */
        {

            int8_t cNew = 0;

            /* build binary from gray */
            if(ENC_PIN_A  != 0){
                cNew = 3;
            }
            if(ENC_PIN_B != 0){
                cNew ^= 1;
            }
            /* shift old value up */
            cLast = (cLast << 2)  & 0x0F;
            /* add new binary value */
            cLast += cNew;

            cEncDelta -= iEncTable[cLast];

            /* Encoder-Taster Pr�fen */
            uButtonState = (ENC_BUTTON == 0);/*Invertierung des Tasters korrigieren*/
            if(uButtonState){
                /* Dauer des Knopf Dr�ckens z�hlen */
                uButtonPressCnt++;
            }
            if((uButtonState != uButtonStateOld) && (uButtonState == 0)){
                /* negative Flanke am Taster */
                if(uButtonPressCnt < ENC_BUTTON_PRESS_THRESHOLD){
                    /* Kurzer Tastendruck */
                    bEncBtnShrtPrs = true;
                }
                uButtonPressCnt = 0;
                bEncBtnLngPrsOld = false;
            }

            if(uButtonPressCnt >= ENC_BUTTON_PRESS_THRESHOLD){
                /* Ein Langer Tastendruck wurde erkannt */
                if(bEncBtnLngPrsOld == false){
                    bEncBtnLngPrs = true;
                    bEncBtnLngPrsOld = true;
                }
            }
            uButtonStateOld = uButtonState;
        }
#endif
        /* count timer interrupts */
        uCycleCount++;

        /* determine 1s clock */
        if(uCycleCount >= ENC_RATE ){
            uCycleCount = 0;
            /* raise flag for 1s */
            bOneSec = true;
            /* check for running temp conversion */
            if(bRunConv){
                bRunConv = false;
            }
        }
    }
    

}
#endif

#ifdef	__cplusplus
}
#endif

#endif /*INTERRUPTS_C */