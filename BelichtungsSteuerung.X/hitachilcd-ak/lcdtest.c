#include <pic.h>
#include "defs.h"
#include "lcd.h"
#include "delay.h"

void main (void)
	{
	UINT	i;
	byte	j;
	const byte const UpArrow[] = {0x04, 0x0e, 0x15, 0x04, 0x04, 0x04, 0x04, 0x04};
	const byte const DownArrow[] = {0x04, 0x04, 0x04, 0x04, 0x04, 0x15, 0x0e, 0x04};

	TRISB = 0;
	PORTB = 0;

	lcd_init ();
	lcd_clear ();
	lcd_command (LCD_COMMAND_HOME);				// Home cursor

	lcd_define_char (0, UpArrow);				// Define user-defined char
	lcd_define_char (1, DownArrow);				// Define user-defined char

	lcd_gotoxy (1,1);
	lcd_putc (0);
	lcd_gotoxy (2,1);
	lcd_putc (1);						// Display special character


	lcd_gotoxy (1,8);

	for (j = 0; j < 43; j++)
		{
		lcd_putc ('.');
		for (i = 0; i < 1000; i++)
			delay_mS (1);
		}


	NOP ();
	asm ("goto $-1");
	}
