#ifndef DELAY_H
#define	DELAY_H

#define	FOSC		4000000				// Define your xtal here.  Enter xtal speed!

#define	delay_uS(x)	{asm("rept "#x);asm("nop");asm("endm");}

void delay_mS (byte mS);

#endif

