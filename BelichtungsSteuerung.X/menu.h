/* 
 * File:   menu.h
 * Author: A
 *
 * Created on 20. April 2013, 20:21
 */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef MENU_H
#define	MENU_H

#ifdef	__cplusplus
extern "C" {
#endif
/* Includes ------------------------------------------------------------------*/
#include "system.h"
/* Exported types ------------------------------------------------------------*/
typedef struct{
    int16_t UvTime;     /* seconds Uv lights are on in operation */
    int16_t WarmUpTemp;  /* heat up end temprature  */
    int16_t MinUvTemp;  /* min temp for UV operation */
    int16_t UvHardenTime;/* time for UV hardening in 0.5 min */
    bool TempSensor;    /* Activate/deaktivate Temp handling */
    bool HoodSwitch;    /* Uv off on open hood on/off */
    bool Beep;          /* Beep on/Off */
    int16_t UvRowMinTime; /* start time for Uv series*/
    int16_t UvRowMaxTime; /* end time for Uv series*/
    int16_t UvRowStepTime; /* step time for Uv series*/
}sParameters;
/* Exported constants --------------------------------------------------------*/
extern eeprom sParameters para;
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
/*******************************************************************************
* FUNCTION:   mnu_DoMenu
* PURPOSE:    main menu
*
* INPUT:      -
* OUTPUT:     -
* RETURN:     -
******************************************************************************/
inline void mnu_DoMenu(void);

#ifdef	__cplusplus
}
#endif

#endif	/* MENU_H */

