/*
 * File:   menu.h
 * Author: A
 *
 * Created on 20. April 2013, 20:21
 */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef MENU_C
#define	MENU_C

#ifdef	__cplusplus
extern "C" {
#endif
/* Includes ------------------------------------------------------------------*/
#include "menu.h"
#include "lcd.h"
#include "encoder.h"
#include "ds1820.h"

/* Private typedef -----------------------------------------------------------*/
typedef enum {
    WARP_BOTTOM = 0,
    WARMUP,
    UV_OPERATION,
    UV_HARDEN,
    POSITION,
    SETUP,
    UV_ROW,
    WARP_TOP,
    SETUP_BOTTOM,
    UV_TIME,
    WARMUP_TEMP,
    MIN_UV_TEMP,
    HARDEN_TIME,
    BEEP,
    TEMP_SENSOR,
    HOOD_SWITCH,
    UV_ROW_MIN_TIME,
    UV_ROW_MAX_TIME,
    UV_ROW_STEP_TIME,
    BACK,
    SETUP_TOP
}eMainMenuPages;


typedef struct{
    int16_t UvTime;     /* seconds Uv lights are on in operation */
    int16_t WarmUpTemp;  /* heat up end temprature  */
    int16_t MinUvTemp;  /* min temp for UV operation */
    int16_t UvHardenTime;/* time for UV hardening in 0.5 min */
    bool TempSensor;    /* Activate/deaktivate Temp handling */
    bool HoodSwitch;    /* Uv off on open hood on/off */
    bool Beep;          /* Beep on/Off */
    int16_t UvRowMinTime; /* start time for Uv series*/
    int16_t UvRowMaxTime; /* end time for Uv series*/
    int16_t UvRowStepTime; /* step time for Uv series*/
}sParameters;

typedef struct{
    int16_t  UvTime;     /* seconds Uv lights are on remaining */
    int16_t WarmUpTemp;  /* actual temprature  */
    int16_t WarmUpTime;  /* actual time in Warmup */
    bool     WarmUp;      /* Warmup status */
    bool     UvLight;     /* Uv lights status */
    bool     UvHarden;    /* Uv harden mode*/
    bool     Hood;        /* hood status */
    bool     Edit;        /* Options Edit status */
    bool     Setup;       /* Menu in Setup level */
    bool     PosLight;    /* position lights status */
    bool     Redraw;      /* redraw menu flag */
    bool     UVRow;       /* UV Row status */
    int16_t  UVRowTime;   /* UVRow time all over*/
    eMainMenuPages Index; /* main menu index */
}sData;

/* Private define ------------------------------------------------------------*/
#define UV_LIGHT_TIME_MAX               1800 /* 30 minutes */
/* Private macro -------------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
inline void mnu_UvOperation(void);
inline void mnu_UvRow(void);
inline void mnu_WarmUp(void);
#if 0
inline void mnu_Setup(void);
#endif
inline bool mnu_SetupOption(const char* paraName, const char* unit, int16_t *para,
                    int16_t min, int16_t max, int16_t step, int16_t scale);
inline bool mnu_SetupOptionBool( const char* paraName, bool *para);
/* Private variables ---------------------------------------------------------*/
/* Parametervalues in EEPROM */
eeprom sParameters para = {90,35,30,600,true,true,true,5,35,5};
/* Working data */
volatile sData data = {90,21,0,false,false,false,false,false,false,false,
                       true,false,0,WARMUP};
volatile int8_t iEncRead = 0;
/* buffers */
char dispBuffer[21] = {0};
char tempBuffer[6] = {0};

/* Private functions ---------------------------------------------------------*/

/*******************************************************************************
 * FUNCTION:   mnu_UvOperation
 * PURPOSE:    UV Lighter Operation
 *
 *             - turn UV lights on/off - long press
 *             - turn position lights on/off - short press
 *             - see remaining UvTime
 *             - see actual lights temperature
 *
 * INPUT:      -
 * OUTPUT:     -
 * RETURN:     -
 ******************************************************************************/
inline void mnu_UvOperation(void){

    /* check for key press */
    if(Encoder_ShrtBtnPress()){
        data.PosLight = ~data.PosLight; /* toggle poslights */
    }

    /* check for long key press */
    if(Encoder_LngBtnPress()){
        data.Redraw = true;
        data.UvLight = ~data.UvLight;  /* toggle uvlights */
        /* set actual uv time to parameter value on pos edge */
        if(data.UvLight){
            data.UvTime = para.UvTime;
            data.PosLight = false; /* turn pos lights off cause not needed*/
            /* hood open no UV, when parameter is set */
            if((data.Hood == 0) && para.HoodSwitch){
                data.UvLight = false;
                data.Redraw = false;
                /* print error msg */
                lcd_gotoxy(2,1);
                lcd_printf("  Bitte Deckel zu!  ");
            }
        }
    }

   /* switch menu screens */
    iEncRead = Encoder_Read();
    if(iEncRead != 0){
        data.Index += iEncRead;
        return;  /* screen change */
    }


    /* check for needed redraw */
    if(data.Redraw){
        data.Redraw = false;
        /* redraw display */
        lcd_clear();
        lcd_gotoxy(1,1);
        lcd_printf("Belichtung: ");
        /* draw contex specific */
        if(data.UvLight){
            sprintf(dispBuffer, "%03d Sec.", data.UvTime);
            lcd_printf(dispBuffer);
        }else{
            lcd_printf(" inaktiv");
        }
        lcd_gotoxy(2,1);
        sprintf(dispBuffer,"Temperatur:    %02d C.",data.WarmUpTemp);
        lcd_printf(dispBuffer);
    }
}
/*******************************************************************************
 * FUNCTION:   mnu_UvRow
 * PURPOSE:    UV Lighter Operation as row
 *
 *             - start UV row - long press
 *             - turn UV lights on - short press
 *             - see remaining UvTime
 *             - see actual Row time
 *
 * INPUT:      -
 * OUTPUT:     -
 * RETURN:     -
 ******************************************************************************/
inline void mnu_UvRow(void){


    /* check for key press */
    if(Encoder_ShrtBtnPress()){
        if((data.UvLight == false)&&(data.UVRow)){
            data.Redraw = true;
            data.UvLight = true;  /* turn on uvlights */
            /* set actual uv time to parameter value on pos edge */
            data.UvTime = para.UvRowStepTime;
            data.PosLight = false; /* turn pos lights off cause not needed*/
            /* hood open no UV, when parameter is set */
            if((data.Hood == 0) && para.HoodSwitch){
                data.UvLight = false;
                data.Redraw = false;
                /* print error msg */
                lcd_gotoxy(2,1);
                lcd_printf("  Bitte Deckel zu!  ");
            }else{
                /* count actual time */
                data.UVRowTime += para.UvRowStepTime;
            }
        }
    }

    /* check for long key press */
    if(Encoder_LngBtnPress()){
        data.Redraw = true;
        if(data.UVRow){
            /* reset uv row */
            data.UVRow = false;
            /* set actual uv time to parameter value on pos edge */
            data.UvLight = false;  /* turn on uvlights */
            data.UVRowTime = 0;
        }else{
            data.UVRow = true;
            /* set actual uv time to parameter value on pos edge */
            data.UvLight = true;  /* turn on uvlights */
            /* set actual uv time to parameter value on pos edge */
            data.UvTime = para.UvRowMinTime;
            data.PosLight = false; /* turn pos lights off cause not needed*/
            /* hood open no UV, when parameter is set */
            if((data.Hood == 0) && para.HoodSwitch){
                data.UVRow = false;
                data.UvLight = false;
                data.Redraw = false;
                /* print error msg */
                lcd_gotoxy(2,1);
                lcd_printf("  Bitte Deckel zu!  ");
            }else{
                /* count actual time */
                data.UVRowTime = para.UvRowMinTime;
            }
        }
    }

    /* switch menu screens */
    iEncRead = Encoder_Read();
    if(iEncRead != 0){
        /* only move the screen when no Uv row is active */
        if(data.UVRow == false){
            data.Index += iEncRead;
            return;  /* screen change */
        }
    }

    /* check for end of row */
    if((data.UVRowTime >= para.UvRowMaxTime)&&(data.UvLight == false)){
         /* reset uv row */
        data.UVRow = false;
        /* set actual uv time to parameter value on pos edge */
        data.UVRowTime = 0;
    }

    /* check for needed redraw */
    if(data.Redraw){
        data.Redraw = false;
        /* redraw display */
        lcd_clear();
        lcd_gotoxy(1,1);
        lcd_printf("Belichtungsreihe:");
        /* draw contex specific */
        if(data.UVRow){
            sprintf(dispBuffer, "%02ds", data.UvTime);
            lcd_printf(dispBuffer);
        }else{
            lcd_printf("aus");
        }
        lcd_gotoxy(2,1);
        sprintf(dispBuffer,"Bel.-Schritt:   %03ds",data.UVRowTime);
        lcd_printf(dispBuffer);
    }
}

/*******************************************************************************
 * FUNCTION:   mnu_UvHarden
 * PURPOSE:    UV harden
 *
 *             - turn UV lights on/off - long press
 *             - turn position lights on/off - short press
 *             - see remaining UvTime
 *             - see actual lights temperature
 *
 * INPUT:      -
 * OUTPUT:     -
 * RETURN:     -
 ******************************************************************************/
inline void mnu_UvHarden(void){

    /* check for key press */
    if(Encoder_ShrtBtnPress()){
        data.PosLight = ~data.PosLight; /* toggle poslights */
    }

    /* check for long key press */
    if(Encoder_LngBtnPress()){
        data.UvLight = ~data.UvLight;  /* toggle uvlights */
        /* set actual uv time to parameter value on pos edge */
        if(data.UvLight){
            data.UvTime = para.UvHardenTime;
            data.PosLight = false; /* turn pos lights off cause not needed*/
        }
        data.Redraw = true;
    }

   /* switch menu screens */
    iEncRead = Encoder_Read();
    if(iEncRead != 0){
        data.Index += iEncRead;
        return;  /* screen change */
    }


    /* check for needed redraw */
    if(data.Redraw){
        data.Redraw = false;
        /* redraw display */
        lcd_clear();
        lcd_gotoxy(1,1);
        lcd_printf("UV-Haerten: ");
        /* draw contex specific */
        if(data.UvLight){
            sprintf(dispBuffer, "%03d Sec.", data.UvTime);
            lcd_printf(dispBuffer);
        }else{
            lcd_printf(" inaktiv");
        }
        lcd_gotoxy(2,1);
        if(para.TempSensor){
            sprintf(dispBuffer,"Temperatur:    %02d C.",data.WarmUpTemp);
            lcd_printf(dispBuffer);
        }
    }
}

/*******************************************************************************
 * FUNCTION:   mnu_PositionLight
 * PURPOSE:    position lights only
 *
 *             - turn position lights on/off - short press
 *             - see actual lights temperature
 *
 * INPUT:      -
 * OUTPUT:     -
 * RETURN:     -
 ******************************************************************************/
inline void mnu_PositionLight(void){

    /* check for key press */
    if(Encoder_ShrtBtnPress()){
        data.PosLight = ~data.PosLight; /* toggle poslights */
        data.Redraw = true;
    }

    /* switch menu screens */
    iEncRead = Encoder_Read();
    if(iEncRead != 0){
        data.Index += iEncRead;
        data.Redraw = true;
        return;  /* screen change */
    }


    /* check for needed redraw */
    if(data.Redraw){
        data.Redraw = false;
        /* redraw display */
        lcd_clear();
        lcd_gotoxy(1,1);
        lcd_printf("Positionieren:      ");
        lcd_gotoxy(2,1);
        lcd_printf("Positions-LEDs:");
        /* draw contex specific */
        if(data.PosLight){
            lcd_printf("   an");
        }else{
            lcd_printf("  aus");
        }   
    }
}

/*******************************************************************************
 * FUNCTION:   mnu_WarmUp
 * PURPOSE:    Warmup UV lights
 *
 *             - turn on/off UV lights warmup - long press
 *             - turn position lights on/off - short press
 *             - see actual lights temperature
 *             - see actual time in warmup
 *
 * INPUT:      -
 * OUTPUT:     -
 * RETURN:     -
 ******************************************************************************/
inline void mnu_WarmUp(void){

    /* check for key press */
    if(Encoder_ShrtBtnPress()){
        data.PosLight = ~data.PosLight; /* toggle poslights */
    }

    /* check for long key press */
    if(Encoder_LngBtnPress()){
        data.WarmUp = ~data.WarmUp;  /* toggle uvlights */
        data.Redraw = true;
    }

    /* switch menu screens */
    iEncRead = Encoder_Read();
    if(iEncRead != 0){
        data.Index += iEncRead;
        return;  /* screen change */
    }

    /* check for needed redraw */
    if(data.Redraw){
        data.Redraw = false;
        /* redraw display */
        lcd_clear();
        lcd_gotoxy(1,1);
        lcd_printf("Aufheizen:  ");
        /* draw contex specific */
        if(data.WarmUp){
            sprintf(dispBuffer, "%03d Sec.", data.WarmUpTime);
            lcd_printf(dispBuffer);
        }else{
            lcd_printf(" inaktiv");
        }
        lcd_gotoxy(2,1);
        sprintf(dispBuffer,"Temperatur:    %02d C.",data.WarmUpTemp);
        lcd_printf(dispBuffer);
    }
}

#if 0
/*******************************************************************************
 * FUNCTION:   mnu_Setup
 * PURPOSE:    setup menu
 *
 *             - get in setup menu - short press
 *             - get out of setup menu - long press
 *
 * INPUT:      -
 * OUTPUT:     -
 * RETURN:     -
 ******************************************************************************/
inline void mnu_Setup(void){
    static bool changed = false;
    static bool bValue = false;
    static int16_t iValue = 0;
    
     /* check for key press */
    if(Encoder_ShrtBtnPress()){
        data.Redraw = true;
        /* get deeper in the setup menu */
        if(data.Setup){
            data.Edit = true;
        }else{
            data.Setup = true;
        }
    }

    /* check for long key press */
    if(Encoder_LngBtnPress()){
        data.Redraw = true;
        /* go back to main mainmenu */
        data.Edit = false;
        data.Setup = false;
    }

   
    if(data.Setup) {
        /* Setup menu */
        switch(Index){
             /* wrap around at bottom */
            case SETUP_BOTTOM:
                data.Index = SETUP_TOP - 1; /* last real menu entry */
                break;

            /* 
             * UV lighter time
             */
            case UV_TIME:
                iValue = para.UvTime;

                changed = mnu_SetupOption("Belichtungszeit:    ","Sec ",
                                          &iValue, 0, 600, 1, 1);
                if(changed){
                    para.UvTime = iValue;
                }
                break;

            /*
             * Warmup temperature for UV Lights
             */
            case WARMUP_TEMP:
                iValue = para.WarmUpTemp;

                changed = mnu_SetupOption("Aufwaermtemperatur: ","Grad",
                                &iValue, 15, 80, 1, 1);
                if(changed){
                    para.WarmUpTemp = iValue;
                }
                break;

            /*
             * Min operational temperature for UV Lights
             */
            case MIN_UV_TEMP:
                iValue = para.MinUvTemp;

                changed = mnu_SetupOption("Min. UV-Temperatur: ","Grad",
                                &iValue, 15, 60, 1, 1);
                if(changed){
                    para.MinUvTemp = iValue;
                }
                break;

            /*
             * Time to harden with UV
             */
            case HARDEN_TIME:
                iValue = para.UvHardenTime;

                changed = mnu_SetupOption("UV-Haertezeit:      ","Min ",
                                &iValue,0, 30, 1, 60);
                if(changed){
                    para.UvHardenTime = iValue;
                }
                break;

            /*
             * turn on off beep signals
             */
            case BEEP:
                bValue = para.Beep;

                changed = mnu_SetupOptionBool("Summer-Signale:     ",&bValue);

                if(changed){
                    para.Beep = bValue;
                }
                break;

            /*
             * master switch for temperature sensor use
             */
            case TEMP_SENSOR:
                bValue = para.TempSensor;

                changed = mnu_SetupOptionBool("Temperatursensor:    ",&bValue);
                
                if(changed){
                    para.TempSensor = bValue;
                }
                break;

            /*
             * turn on off hood switch check
             */
            case HOOD_SWITCH:
                bValue = para.HoodSwitch;

                changed = mnu_SetupOptionBool("Klappenkontakt:     ", &bValue);
                
                if(changed){
                    para.HoodSwitch = bValue;
                }
                break;

            /*
             * start time for uv row
             */
            case UV_ROW_MIN_TIME:
                iValue = para.UvRowMinTime;

                changed = mnu_SetupOption("Bel.-Reihe Startzeit","Sec ",
                                &iValue, 0, para.UvRowMaxTime, 1, 1);
                if(changed){
                    para.UvRowMinTime = iValue;
                }
                break;

            /*
             * end time for uv row
             */
            case UV_ROW_MAX_TIME:
                iValue = para.UvRowMaxTime;

                changed = mnu_SetupOption("Bel.-Reihe Stopzeit ","Sec ",
                                &iValue, para.UvRowMinTime, 600, 1, 1);
                if(changed){
                    para.UvRowMaxTime = iValue;
                }
                break;

            /*
             * step size for UV row steps
             */
            case UV_ROW_STEP_TIME:
                iValue = para.UvRowStepTime;

                changed = mnu_SetupOption("Bel.-Reihe Schritt  ","Sec ",
                                &iValue,1,
                                (para.UvRowMaxTime - para.UvRowMinTime), 1, 1);
                if(changed){
                    para.UvRowStepTime = iValue;
                } 
                break;

            /* wrap at the top */
            case SETUP_TOP:
                data.Setup = SETUP_BOTTOM + 1; /* goto first menu point */
                break;

            /* default case */
            default:
                data.Setup = SETUP_BOTTOM + 1; /* goto first menu point */
                break;
        }

    }
    /* switch menu screens */
    else {
        /* only check encoder when not in setup menu */
        iEncRead = Encoder_Read();
        if(iEncRead != 0){
            data.Index += iEncRead;
            return;  /* screen change */
        }
        /* check for needed redraw , when not in menu */
        if(data.Redraw){
            data.Redraw = false;
            /* redraw display */
            lcd_clear();
            lcd_gotoxy(1,1);
            lcd_printf("       SETUP        ");
         }
    }
}
#endif

/*******************************************************************************
 * FUNCTION:   mnu_SetupOption
 * PURPOSE:    Setup various parameters
 *
 *             - set parameter - short press
 *
 * INPUT:      -
 * OUTPUT:     -
 * RETURN:     -
 ******************************************************************************/
inline bool mnu_SetupOption(const char* paraName, const char* unit, int16_t* para,
                    int16_t min, int16_t max, int16_t step, int16_t scale){

    static int16_t iValue = 0;
    static bool changed = false;
    static bool oldEdit = false;

    /* reset flag in next cycle */
    if(changed){
        changed = false;
    }
    /* on edge */
    if((data.Edit!=oldEdit)){
        if(oldEdit){
            /* falling edge of edit */
            if(*para != iValue*scale){
                /* scaled by scale value */
                *para = iValue*scale;
                changed = true;
            }
        }else{
            /* rising edge of edit */
            iValue = *para/scale;
        }
    }
    oldEdit = data.Edit;

    /* check parameter bounds */
    if(iValue < min){
        iValue = min;
    }else if(iValue > max){
        iValue = max;
    }
    /* switch menu screens, or change parameter */
    iEncRead = Encoder_Read();
    if(iEncRead != 0){
        data.Redraw = true;
        if(data.Edit){
            /* turn parameter by encoder with step as width*/
            iValue+= (int16_t)iEncRead*step;
         }else{
            data.Index += iEncRead;
            return changed;  /* screen change */
        }
    }


    /* check for needed redraw */
    if(data.Redraw){
        data.Redraw = false;
        /* redraw display */
        lcd_clear();
        lcd_gotoxy(1,1);
        lcd_printf(paraName);
        lcd_gotoxy(2,1);
        /* draw contex specific */
        if(data.Edit){
            sprintf(dispBuffer, "->          %03d %s", iValue, unit);
        }else{
            sprintf(dispBuffer, "            %03d %s", (*para)/scale, unit);
        }
        lcd_printf(dispBuffer);
    }
    return changed;
}


/*******************************************************************************
 * FUNCTION:   mnu_SetupOption
 * PURPOSE:    Setup various parameters
 *
 *             - set parameter - short press
 *
 * INPUT:      -
 * OUTPUT:     -
 * RETURN:     -
 ******************************************************************************/
inline bool mnu_SetupOptionBool(const char* paraName, bool* para){

    static bool bValue = 0;
    static bool changed = false;
    static bool oldEdit = false;
    
 /* reset flag in next cycle */
    if(changed){
        changed = false;
    }
    /* on edge */
    if((data.Edit!=oldEdit)){
        if(oldEdit){
            /* falling edge of edit */
            if(*para != bValue){
                /* scaled by scale value */
                *para = bValue;
                changed = true;
            }
        }else{
            /* rising edge of edit */
            bValue = *para;
        }
    }
    oldEdit = data.Edit;

    /* switch menu screens, or change parameter */
    iEncRead = Encoder_Read();
    if(iEncRead != 0){
        data.Redraw = true;
        if(data.Edit){
            /* turn parameter by encoder*/
            bValue = (~bValue)&0x01;
         }else{
            data.Index += iEncRead;
            /* set parameter on leave of the parameter */
            /* secure mc lib error define of true as 1 instead of 0xFF */
            if(bValue > true){
                bValue = true;
            }
            *para = bValue;
            return changed;  /* screen change */
        }
    }

    /* check for needed redraw */
    if(data.Redraw){
        data.Redraw = false;
        /* redraw display */
        lcd_clear();
        lcd_gotoxy(1,1);
        lcd_printf(paraName);
        lcd_gotoxy(2,1);
        /* draw contex specific */
        if(data.Edit){
            lcd_printf("->          ");
            if(bValue){
                lcd_printf("aktiv   ");
            }else{
                lcd_printf("inaktiv ");
            }
        }else{
            lcd_printf("            ");
            if(*para){
                lcd_printf("aktiv   ");
            }else{
                lcd_printf("inaktiv ");
            }
        }

    }
    return changed;
}


/* Global functions ----------------------------------------------------------*/
/*******************************************************************************
* FUNCTION:   mnu_DoMenu
* PURPOSE:    main menu
*
* INPUT:      -
* OUTPUT:     -
* RETURN:     -
******************************************************************************/
inline void mnu_DoMenu(void){
    static bool changed = false;
    static bool bValue = false;
    static int16_t iValue = 0;
    
    switch(data.Index){
        /* wrap around at bottom */
        case WARP_BOTTOM:
            data.Index = WARP_TOP - 1; /* last real menu entry */
            break;
            
        /* UV Lighter Operation
         * 
         * turn UV lights on/off - long press
         * turn position lights on/off - short press
         * see remaining UvTime
         * see actual lights temperature
         */
        case UV_OPERATION:
            mnu_UvOperation();
            break;

        /* UV Lighter Operation as Row
         *
         * start UV row - long press
         * turn UV lights on - short press
         * see remaining UvTime
         * see actual Row time
         */
        case UV_ROW:
            mnu_UvRow();
            break;

        /* UV Harden
         *
         * turn UV lights on/off - long press
         * turn position lights on/off - short press
         * see remaining UvTime
         * see actual lights temperature
         */
        case UV_HARDEN:
            mnu_UvHarden();
            break;

        /* POSITION
         *
         * turn position lights on/off - short press
         */
        case POSITION:
            mnu_PositionLight();
            break;

        /* Warmup UV lights
         *
         * turn on/off UV lights warmup - short press
         * see actual lights temperature
         * see actual time in warmup
         */
        case WARMUP:
            if(para.TempSensor){
                mnu_WarmUp();
            } else {
                /* step ahead when T-Sensor is disabled*/
                data.Index++;
            }
            break;

         /* Setup
          *
          * get in setup menu - short press
          * 
          */
        case SETUP:
            if(data.Setup){
                /* jumps to the first element in setup menu */
                data.Index = SETUP_BOTTOM + 1; 
            }else{
                /* check for needed redraw , when not in menu */
                if(data.Redraw){
                    data.Redraw = false;
                    /* redraw display */
                    lcd_clear();
                    lcd_gotoxy(1,1);
                    lcd_printf("       SETUP        ");
                }
                   /* check for key press */
                if(Encoder_ShrtBtnPress()){
                    data.Redraw = true;
                    /* get deeper in the setup menu */
                    data.Setup = true;
                }
                /* only check encoder when not in setup menu */
                iEncRead = Encoder_Read();
                if(iEncRead != 0){
                    data.Index += iEncRead;
                    data.Redraw = true;
                }
            }

            
            //mnu_Setup();
            break;
                 
        /* wrap at the top */
        case WARP_TOP:
            data.Index = WARP_BOTTOM + 1; /* goto first menu point */
            break;

/*----------------------------------------------------------------------------*/
/*                          Setup Menu                                        */
/*----------------------------------------------------------------------------*/

        /* wrap around at bottom */
        case SETUP_BOTTOM:
            data.Index = SETUP_TOP - 1; /* last real menu entry */
            break;

        /*
         * UV lighter time
         */
        case UV_TIME:
            iValue = para.UvTime;

            changed = mnu_SetupOption("Belichtungszeit:    ","Sec ",
                                      &iValue, 0, 600, 1, 1);
            if(changed){
                para.UvTime = iValue;
            }
            break;

        /*
         * Warmup temperature for UV Lights
         */
        case WARMUP_TEMP:
            iValue = para.WarmUpTemp;

            changed = mnu_SetupOption("Aufwaermtemperatur: ","Grad",
                            &iValue, 15, 80, 1, 1);
            if(changed){
                para.WarmUpTemp = iValue;
            }
            break;

        /*
         * Min operational temperature for UV Lights
         */
        case MIN_UV_TEMP:
            iValue = para.MinUvTemp;

            changed = mnu_SetupOption("Min. UV-Temperatur: ","Grad",
                            &iValue, 15, 60, 1, 1);
            if(changed){
                para.MinUvTemp = iValue;
            }
            break;

        /*
         * Time to harden with UV
         */
        case HARDEN_TIME:
            iValue = para.UvHardenTime;

            changed = mnu_SetupOption("UV-Haertezeit:      ","Min ",
                            &iValue,0, 30, 1, 60);
            if(changed){
                para.UvHardenTime = iValue;
            }
            break;

        /*
         * turn on off beep signals
         */
        case BEEP:
            bValue = para.Beep;

            changed = mnu_SetupOptionBool("Summer-Signale:     ",&bValue);

            if(changed){
                para.Beep = bValue;
            }
            break;

        /*
         * master switch for temperature sensor use
         */
        case TEMP_SENSOR:
            bValue = para.TempSensor;

            changed = mnu_SetupOptionBool("Temperatursensor:    ",&bValue);

            if(changed){
                para.TempSensor = bValue;
            }
            break;

        /*
         * turn on off hood switch check
         */
        case HOOD_SWITCH:
            bValue = para.HoodSwitch;

            changed = mnu_SetupOptionBool("Klappenkontakt:     ", &bValue);

            if(changed){
                para.HoodSwitch = bValue;
            }
            break;

        /*
         * start time for uv row
         */
        case UV_ROW_MIN_TIME:
            iValue = para.UvRowMinTime;

            changed = mnu_SetupOption("Bel.-Reihe Startzeit","Sec ",
                            &iValue, 0, para.UvRowMaxTime, 1, 1);
            if(changed){
                para.UvRowMinTime = iValue;
            }
            break;

        /*
         * end time for uv row
         */
        case UV_ROW_MAX_TIME:
            iValue = para.UvRowMaxTime;

            changed = mnu_SetupOption("Bel.-Reihe Stopzeit ","Sec ",
                            &iValue, para.UvRowMinTime, 600, 1, 1);
            if(changed){
                para.UvRowMaxTime = iValue;
            }
            break;

        /*
         * step size for UV row steps
         */
        case UV_ROW_STEP_TIME:
            iValue = para.UvRowStepTime;

            changed = mnu_SetupOption("Bel.-Reihe Schritt  ","Sec ",
                            &iValue,1,
                            (para.UvRowMaxTime - para.UvRowMinTime), 1, 1);
            if(changed){
                para.UvRowStepTime = iValue;
            }
            break;

        case BACK:
            /* check for needed redraw , when not in menu */
            if(data.Redraw){
                data.Redraw = false;
                /* redraw display */
                lcd_clear();
                lcd_gotoxy(1,1);
                lcd_printf("<-   Hauptmenue     ");
            }
               /* check for key press */
            if(Encoder_ShrtBtnPress()){
                data.Redraw = true;
                /* go back to main mainmenu */
                data.Edit = false;
                data.Setup = false;
                data.Index = SETUP;
            }
            
            iEncRead = Encoder_Read();
            if(iEncRead != 0){
                data.Index += iEncRead;
                data.Redraw = true;
            }
            break;
        /* wrap at the top */
        case SETUP_TOP:
            data.Index = SETUP_BOTTOM + 1; /* goto first menu point */
            break;

        /* default case */
        default:
            data.Index = WARP_BOTTOM + 1; /* goto first menu point */
            break;
    }

    /* check outputs */
    if(data.UvLight || data.WarmUp){
        OUT_UV_LIGHT = 1;
    }else{
        OUT_UV_LIGHT = 0;
    }

    if(data.PosLight){
        OUT_POS_LIGHT = 1;
    }else{
        OUT_POS_LIGHT = 0;
    }

    /* check inputs */
    data.Hood = (IN_HOOD==0);
    OUT_LED2 = (data.Hood==0);
    OUT_LED3 = (IN_RESERVE==1);

    /* 1 sec tick */
    if(bOneSec){
        bOneSec = false;
        /* get new temp from ds18s20 */
        data.WarmUpTemp = DS1820_GetTempRaw();
        /* start next conversion */
        DS1820_StartTempConv();
        /* redraw new temp minimum */
        data.Redraw = true;
        /* lighter mode */
        if(data.UvLight){
            /* decreese time */
            data.UvTime--;
            data.Redraw = true;
            /* uv lighting finished */
            if(data.UvTime <= 0){
                data.UvTime = 0;
                data.UvLight = false;
                /* beep */
                OUT_BUZZER = para.Beep&0x01;
                __delay_ms(100);
                OUT_BUZZER = 0;
            }
        }
        /* warmup mode */
        if(data.WarmUp){
            /* increese time */
            data.WarmUpTime++;
            if(data.WarmUpTemp >= para.WarmUpTemp){
                /* warmup finished */
                /* turn Light off */
                data.WarmUp = false;
                data.WarmUpTime = 0;
                /* beep */
                OUT_BUZZER = para.Beep&0x01;
                __delay_ms(100);
                OUT_BUZZER = 0;
            }
            data.Redraw = true;
        }

        /* setup mode */
        if(data.Setup){
             /* check for key press */
            if(Encoder_ShrtBtnPress()){
                data.Redraw = true;
                /* get deeper in the setup menu */
                data.Edit = (~data.Edit)&0x01;
            }

            /* check for long key press */
            if(Encoder_LngBtnPress()){
                data.Redraw = true;
                /* go back to main mainmenu */
                data.Edit = false;
                data.Setup = false;
                data.Index = SETUP;
            }
        }

        
    }    
}

#endif	/* MENU_C */
